            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Daftar Buku
                        <small>Perpustakaan Digital</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> <a href="dashboard.php">Dashboard</a></li>
                        <li class="active">Buku</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-book"></i> Daftar Buku</h3>
                                    <div class="margin box-tools">

                                        <div class="input-group">
                                            <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                                           
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-default" ><i class="fa fa-search"></i> Cari Buku</button>
                                                <a class="btn btn-sm btn-primary btn-flat" data-toggle="modal" data-target="#tambah-modal" title="Tambah Buku" style="color:#fff;"><i class="fa fa-plus"></i> Tambah Buku</a>
                                                <button class="btn btn-sm btn-danger btn-flat" title="Delete"><i class='fa fa-trash-o'></i> Delete (buku yg dipilih)</button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="tblBuku" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center"><input type="checkbox" id="check-all" class=""/></th>
                                                <th class="text-center">Cover</th>
                                                <th class="text-center">Judul Buku</th>
                                                <th class="text-center">Kategori</th>
                                                <th class="text-center">Penerbit</th>
                                                <th class="text-center">Penulis</th>
                                                <th class="text-center">Jumlah</th>
                                                <th class="text-center">Stok</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $buku)
                                            <tr>
                                                <td class="text-center"><input type="checkbox" name="buku[]" value="{{ $buku['id'] }}"/></td>
                                                <td class="text-center">
                                                    @if(empty($buku['cover']))
                                                        <img src="{{ asset('assets/img/nocover.png') }}" alt="{{ $buku['judul'] }}" class="img-rounded" style="width: 100px; height: 100px;"/>
                                                    @else
                                                        <img src="{{ asset('uploads/cover/thumb/'.$buku['cover']) }}" class="img-responsive" alt="{{ $buku['judul'] }}" />
                                                    @endif
                                                </td>
                                                <td><a href="#" class="dataEdit" data-type="text" data-pk="{{ $buku['id'] }}" data-url="{{ url('ajax/edit/ajaxbuku') }}" data-name="buku_judul" data-title="Judul Buku">{{ $buku['judul'] }}</a></td>
                                                <td><a href="#" class="dataEditKat" data-type="select" data-pk="{{ $buku['id'] }}" data-url="{{ url('ajax/edit/ajaxbuku') }}" data-name="buku_kategori" data-title="Kategori Buku" data-value="{{ $buku->kategori->id }}">{{ $buku->kategori->nama }}</a></td>
                                                <td><a href="#" class="dataEdit" data-type="text" data-pk="{{ $buku['id'] }}" data-url="{{ url('ajax/edit/ajaxbuku') }}" data-name="buku_penerbit" data-title="Penerbit">{{ $buku['penerbit'] }}</a></td>
                                                <td><a href="#" class="dataEdit" data-type="text" data-pk="{{ $buku['id'] }}" data-url="{{ url('ajax/edit/ajaxbuku') }}" data-name="buku_penulis" data-title="Penulis">{{ $buku['penulis'] }}</a></td>
                                                <td class="text-center"><a href="#" class="dataEditJumlah" data-type="text" data-pk="{{ $buku['id'] }}" data-url="{{ url('ajax/edit/ajaxbuku') }}" data-name="buku_jumlah" data-title="Jumlah Buku">{{ $buku['jumlah'] }}</a></td>
                                                <td class="text-center stock_sisa{{ $buku['id'] }}">{{ $buku['jumlah']-($buku->pinjam->count()) }}</td>
                                                <td class="text-center">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-flat btn-success" title="Pinjam Buku"><i class="fa fa-check-circle-o"></i> Pinjam Buku</button>
                                                        <button type="button" class="btn btn-flat btn-info" title="Edit"><i class='fa fa-pencil'></i> Edit</button>
                                                        <button type="button" class="btn btn-flat btn-danger" title="Delete"><i class='fa fa-trash-o'></i> Delete</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="box-footer clearfix">
                                        @if($data->getLastPage() > 1)
                                        <ul class="pagination pagination-sm no-margin pull-right">
                                            <?php
                                            $presenter = new Illuminate\Pagination\BootstrapPresenter($data);
                                            echo $presenter->render();
                                            ?>
                                        </ul>
                                        @endif
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->

        <!-- Tambah buku modal -->
        <div class="modal fade" id="tambah-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-plus-square-o"></i> Tambah Buku</h4>
                    </div>
                    {{ Form::open(array('id'=>'modalBuku', 'url'=>'ajax/new/buku', 'files'=>true)) }}
                        <div class="modal-body">
                            <div id="bukuResult"></div>
                            <div id="loading" class="text-center" style="display:none;"><img src="{{ asset('assets/img/ajax-loader1.gif') }}" title="Loading..."/><br/>Jangan di tutup sebelum loading selesai...</div>
                            <div id="modalForm">
                            <div class="form-group">
                                <div class="btn btn-success btn-file">
                                    <i class="fa fa-upload"></i> Cover
                                    <input type="file" name="cover" id="coverBuku"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Judul</span>
                                    <input name="judul" type="text" class="form-control" placeholder="Judul Buku">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Kategori</span>
                                    <select class="form-control" name="kategori_id">
                                        @foreach(Kategori::all() as $kategori)
                                            <option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">ISBN</span>
                                    <input name="isbn" type="text" class="form-control" placeholder="ISBN">
                                </div>
                            </div>-->
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Penulis</span>
                                    <input name="penulis" type="text" class="form-control" placeholder="Penulis">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Penerbit</span>
                                    <input name="penerbit" type="text" class="form-control" placeholder="Penerbit">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Tahun Terbit</span>
                                    <input name="thnterbit" type="text" class="form-control tahun-terbit" placeholder="2014">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Jumlah</span>
                                    <input name="jumlah" type="text" class="form-control" placeholder="100">
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="modal-footer clearfix">

                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>

                            <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    {{Form::close()}}
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        @include('pengurus.layouts.footer')
        <script src="http://malsup.github.com/jquery.form.js"></script> 
        <!-- page script -->
        <script type="text/javascript">
            $(document).ready(function() {
                $.fn.editable.defaults.mode = 'inline';
                $('.dataEdit').editable();
                $('.dataEditKat').editable({
                    source: [
                        @foreach(Kategori::all() as $kkat)
                        {value: '{{ $kkat['id'] }}', text: '{{ ucfirst($kkat['nama']) }}'},
                        @endforeach
                    ]
                });
                function processJson(data) { 
                    if(data.fail) {
                        alert(data.msg);
                        $('#modalForm').show();
                        $('#loading').hide();
                        $('#bukuResult').html('<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><b>Gagal menambahkan buku.</b><br/><span id="msgTambahan"></span></div>');
                        $.each(data.msg, function( index, value ) {
                            $('#msgTambahan').append(value + '<br />');
                        });
                    }
                    if(data.success) {
                        $('#modalForm').show();
                        $('#loading').hide();
                        $('#bukuResult').html('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><b>' + data.msg + '</b><br/><span id="msgTambahan"></span></div>');
                    }
                }
                function bef(){
                    $('#modalForm').hide();
                    $('#loading').show();
                }
                $('#modalBuku').ajaxForm({
                    beforeSubmit: bef,
                    dataType: 'json',
                    success: processJson
                });
                $('.dataEditJumlah').editable({
                    success: function(response, newValue) {
                        $('.stock_sisa' + $(this).data('pk')).text(newValue-18);
                    }
                });

                $('#tblBuku').dataTable({
                    "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 0, 1, 8 ] }],
                    "bPaginate": false,
                    "bLengthChange": true,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": true
                });
            });
        </script>