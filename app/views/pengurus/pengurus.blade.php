            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Daftar Pengurus
                        <small>Perpustakaan Digital</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> <a href="dashboard.php">Dashboard</a></li>
                        <li class="active">Pengurus</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-users"></i>  Daftar Pengurus</h3>
                                    <div class="margin box-tools">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-danger btn-flat pull-right" title="Delete"><i class='fa fa-trash-o'></i> Delete (pengurus yg dipilih)</button>
                                                <a class="btn btn-sm btn-primary btn-flat pull-right" title="Tambah Pengurus" data-toggle="modal" data-target="#tambah-modal" style="color:#fff;"><i class="fa fa-plus"></i> Tambah Pengurus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="tblPengurus" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center"><input type="checkbox" class="flat-red"/></th>
                                                <th class="text-center">Username</th>
                                                <th class="text-center">Nama Lengkap</th>
                                                <th class="text-center">Email</th>
                                                <th class="text-center">Foto</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $user)
                                            <tr>
                                                <td class="text-center"><input type="checkbox" name="pengurus[]" value="{{ $user['id'] }}"/></td>
                                                <td><a href="#" class="dataEdit" data-type="text" data-pk="{{ $user['id'] }}" data-url="test.php" data-name="admin_username" data-title="Username">{{ $user['username'] }}</a></td>
                                                <td><a href="#" class="dataEdit" data-type="text" data-pk="{{ $user['id'] }}" data-url="test.php" data-name="admin_nama" data-title="Nama Lengkap">{{ $user['nama'] }}</a></td>
                                                <td><a href="#" class="dataEdit" data-type="text" data-pk="{{ $user['id'] }}" data-url="test.php" data-name="admin_email" data-title="Email">{{ $user['email'] }}</a></td>
                                                <td class="text-center">
                                                    @if(empty($user['foto']))
                                                        <img src="{{ asset('assets/img/avatar4.png') }}" alt="{{ $user['username'] }}" class="img-rounded" style="width: 100px; height: 100px;"/>
                                                    @else
                                                        <img src="{{ asset($user['foto']) }}" class="img-responsive" alt="{{ $user['username'] }}" />
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-flat btn-info" title="Edit"><i class='fa fa-pencil'></i> Edit</button>
                                                        <button type="button" class="btn btn-flat btn-danger" title="Delete"><i class='fa fa-trash-o'></i> Delete</button>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="box-footer clearfix">
                                        @if($data->getLastPage() > 1)
                                        <ul class="pagination pagination-sm no-margin pull-right">
                                            <?php
                                            $presenter = new Illuminate\Pagination\BootstrapPresenter($data);
                                            echo $presenter->render();
                                            ?>
                                        </ul>
                                        @endif
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- Tambah pengurus modal -->
        <div class="modal fade" id="tambah-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-plus-square-o"></i> Tambah Pengurus</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Username</span>
                                    <input name="email_to" type="text" class="form-control" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Password</span>
                                    <input name="email_to" type="password" class="form-control" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Nama</span>
                                    <input name="email_to" type="text" class="form-control" placeholder="Nama Lengkap">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Email</span>
                                    <input name="email_to" type="text" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="btn btn-success btn-file">
                                    <i class="fa fa-upload"></i> Foto Pengurus
                                    <input type="file" name="attachment"/>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer clearfix">

                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>

                            <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        @include('pengurus.layouts.footer')
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $.fn.editable.defaults.mode = 'inline';
                $('.dataEdit').editable();
                $('#tblPengurus').dataTable({
                    "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 0, 4, 5 ] }],
                    "bPaginate": false,
                    "bLengthChange": true,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": true
                });
            });
        </script>