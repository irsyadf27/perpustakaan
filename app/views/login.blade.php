    <body class="bg-black">
    	<section class="content bg-black">
	    	<div class="row">
	    		@if (Session::get('loginError'))
		    	<div class="col-md-12">
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<b>Error!</b> {{ Session::get('loginError') }}
					</div>
				</div>
				@endif
				@foreach ($errors->all() as $error)
			    	<div class="col-md-12">
						<div class="alert alert-warning alert-dismissable">
							<i class="fa fa-ban"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<b>Error!</b> {{  $error }}
						</div>
					</div>
				@endforeach
		        <div class="form-box" id="login-box">
		            <div class="header">{{ $login_header }}</div>
		            {{ Form::open(array('url' => $login_url)) }}
		                <div class="body bg-gray">
		                    <div class="form-group">
		                    	{{ Form::text($input_user, '', array('class'=>'form-control', 'placeholder'=>ucfirst($input_user))) }}
		                    </div>
		                    <div class="form-group">
		                    	{{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
		                    </div>          
		                    <div class="form-group">
		                    	{{ Form::checkbox('remember_me') }} Remember me
		                    </div>
		                </div>
		                <div class="footer">                                                               
		                    <button type="submit" class="btn bg-olive btn-block">Sign me in</button>
		                    <p class="text-center">Perpustakaan Digital {{ $sekolah }}</p>
		                </div>
		            {{ Form::close() }}
		        </div>
	        </div>
        </section>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
