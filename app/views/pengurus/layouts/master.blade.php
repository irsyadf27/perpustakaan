<!DOCTYPE html>
<html>
	@include('pengurus.layouts.head', array('page_title' => (isset($page_title) ? $page_title : '')))
	<body class="skin-blue">
		@include('pengurus.layouts.header')
		@include('pengurus.layouts.sidebar')
		{{ $body }}
	</body>
</html>