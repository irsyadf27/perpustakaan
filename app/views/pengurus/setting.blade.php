            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Pengaturan
                        <small>Perpustakaan Digital</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> <a href="dashboard.php">Dashboard</a></li>
                        <li class="active">Pengaturan</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-6">
                            <!-- Input addon -->
                            <div class="box box-info">
                                <div class="box-header">
                                    <h3 class="box-title">Pengaturan</h3>
                                </div>
                                <div class="box-body">
                                    <div class="input-group">
                                        <span class="input-group-addon">Logo Sekolah</span>
                                        <input class="" type="file">
                                    </div>
                                    <br/>
                                    <div class="input-group">
                                        <span class="input-group-addon">Nama Sekolah</span>
                                        <input type="text" class="form-control" {{ ($data->nama) ? 'value="'.$data->nama.'"' : '' }} placeholder="Nama Sekolah">
                                    </div>
                                    <br/>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        <input type="text" class="form-control" {{ ($data->telp) ? 'value="'.$data->telp.'"' : '' }} placeholder="Telp">
                                    </div>
                                    <br/>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <input type="text" class="form-control" {{ ($data->email) ? 'value="'.$data->email.'"' : '' }} placeholder="Email">
                                    </div>
                                    <br/>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <input type="text" class="form-control" {{ ($data->alamat) ? 'value="'.$data->alamat.'"' : '' }} placeholder="Alamat">
                                    </div>
                                    <br/>
                                    <div class="input-group-btn">
                                        <button class="btn btn-md btn-success btn-flat pull-right" title="Save"><i class="fa fa-save"></i> Simpan</button>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>

                        <div class="col-md-6">
                            <!-- Input addon -->
                            <div class="box box-info">
                                <div class="box-header">
                                    <h3 class="box-title">Denda</h3>
                                </div>
                                <div class="box-body">
                                    <div class="input-group">
                                        <span class="input-group-addon">Denda per Buku/Hari</span>
                                        <input type="text" class="form-control denda" {{ ($data->denda) ? 'value="'.$data->denda.'"' : '' }} placeholder="1000">
                                    </div>
                                    <br/>
                                    
                                        <label>Denda per Buku</label> <input type="checkbox" class="form-control" {{ ($data->denda_perhari == 1) ? 'checked' : '' }}>
                                    
                                    <br/>
                                    <div class="input-group-btn">
                                        <button class="btn btn-md btn-success btn-flat pull-right" title="Save"><i class="fa fa-save"></i> Simpan</button>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        @include('pengurus.layouts.footer')
        <script>
            $(function() {
                $('.denda').priceFormat({
                    prefix: 'Rp. ',
                    centsSeparator: ',',
                    thousandsSeparator: '.',
                    centsLimit: 0
                });
            });
        </script> 