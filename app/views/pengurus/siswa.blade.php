            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Daftar Siswa
                        <small>Perpustakaan Digital</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> <a href="dashboard.php">Dashboard</a></li>
                        <li class="active">Siswa</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-users"></i> Daftar Siswa</h3>
                                    <div class="margin box-tools">

                                        <div class="input-group">
                                            <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                                           
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-default" ><i class="fa fa-search"></i> Cari Siswa</button>
                                                <a class="btn btn-sm btn-primary btn-flat" title="Tambah Siswa" data-toggle="modal" data-target="#tambah-modal" style="color:#fff;"><i class="fa fa-plus"></i> Tambah Siswa</a>
                                                <button class="btn btn-sm btn-danger btn-flat" title="Delete"><i class='fa fa-trash-o'></i> Delete (siswa yg dipilih)</button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="tblSiswa" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center"><input type="checkbox" class="flat-red"/></th>
                                                <th class="text-center">NIS</th>
                                                <th class="text-center">Nama Lengkap</th>
                                                <th class="text-center">Kelas</th>
                                                <th class="text-center">Jenis Kelamin</th>
                                                <th class="text-center">Foto</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $siswa)
                                            <tr>
                                                <td class="text-center"><input type="checkbox" name="siswa[]" value="{{ $siswa['id'] }}" /></td>
                                                <td><a href="#" class="dataEdit" data-type="text" data-pk="{{ $siswa['id'] }}" data-url="test.php" data-name="siswa_nis" data-title="Nomor Induk">{{ $siswa['nis'] }}</a></td>
                                                <td><a href="#" class="dataEdit" data-type="text" data-pk="{{ $siswa['id'] }}" data-url="test.php" data-name="siswa_nama" data-title="Nama Lengkap">{{ ucfirst($siswa['nama']) }}</a></td>
                                                <td><a href="#" class="dataEdit" data-type="text" data-pk="{{ $siswa['id'] }}" data-url="test.php" data-name="siswa_kelas" data-title="Kelas">{{ $siswa['kelas'] }}</a></td>
                                                <td class="text-center"><a href="#" class="dataEditJK" data-type="select" data-pk="{{ $siswa['id'] }}" data-url="test.php" data-name="siswa_jk" data-title="Jenis Kelamin" data-value="{{ $siswa['jeniskelamin'] }}">{{ $siswa['jeniskelamin'] }}</a></td>
                                                <td class="text-center">
                                                    @if(empty($siswa['foto']))
                                                        @if($siswa['jeniskelamin'] == 'L')
                                                            <img src="{{ asset('assets/img/avatar5.png') }}" alt="{{ $siswa['nama'] }}" class="img-rounded" style="width: 100px; height: 100px;"/>
                                                        @else
                                                            <img src="{{ asset('assets/img/avatar2.png') }}" alt="{{ $siswa['nama'] }}" class="img-rounded" style="width: 100px; height: 100px;"/>
                                                        @endif
                                                    @else
                                                        <img src="{{ asset($siswa['foto']) }}" class="img-responsive" alt="{{ $siswa['nama'] }}" />
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-flat btn-info" title="Edit"><i class='fa fa-pencil'></i> Edit</button>
                                                        <button type="button" class="btn btn-flat btn-danger" title="Delete"><i class='fa fa-trash-o'></i> Delete</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="box-footer clearfix">
                                        @if($data->getLastPage() > 1)
                                        <ul class="pagination pagination-sm no-margin pull-right">
                                            <?php
                                            $presenter = new Illuminate\Pagination\BootstrapPresenter($data);
                                            echo $presenter->render();
                                            ?>
                                        </ul>
                                        @endif
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- Tambah siswa modal -->
        <div class="modal fade" id="tambah-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-plus-square-o"></i> Tambah Siswa</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">NIS</span>
                                    <input name="email_to" type="text" class="form-control" placeholder="NIS">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Nama</span>
                                    <input name="email_to" type="text" class="form-control" placeholder="Nama Lengkap">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Kelas</span>
                                    <input name="email_to" type="text" class="form-control" placeholder="XII IPA 3">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Jenis Kelamin</span>
                                    <select name="jeniskelamin" class="form-control">
                                        <option value="L">Laki-Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">No. Telp/HP</span>
                                    <input name="email_to" type="text" class="form-control" placeholder="081XXXXXXXXX">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Password</span>
                                    <input name="email_to" type="password" class="form-control" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="btn btn-success btn-file">
                                    <i class="fa fa-upload"></i> Foto Siswa
                                    <input type="file" name="attachment"/>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer clearfix">

                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>

                            <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        @include('pengurus.layouts.footer')
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $.fn.editable.defaults.mode = 'inline';
                $('.dataEdit').editable();
                $('.dataEditJK').editable({
                    source: [
                        {value: 'L', text: 'L'},
                        {value: 'P', text: 'P'}
                    ]
                });
                $('#tblSiswa').dataTable({
                    "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 0, 5, 6 ] }],
                    "bPaginate": false,
                    "bLengthChange": true,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": true
                });
            });
        </script>