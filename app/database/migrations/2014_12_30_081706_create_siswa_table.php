<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('siswa', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('nis');
			$table->string('nama', 50);
			$table->string('kelas', 10);
			$table->enum('jeniskelamin', array('L', 'P'))->default('L');
			$table->string('telp', 15)->nullable();
			$table->string('password', 64);
			$table->string('foto', 30)->nullable();
			$table->string('remember_token', 100)->nullable();
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('siswa');
	}

}
