<?php

class PengaturanSeeder extends Seeder{

	public function run(){
		DB::table('setting')->delete();
		Setting::create(array(
			'nama'		=> 'SMAN 999 Teladan',
			'telp'		=> '0221213123',
			'email'		=> 'sman999teladan@sekolah.ac.id',
			'alamat'	=> 'Jalan Teladan no. 999',
			'denda'		=> 1000,
			'denda_perhari'	=> 1,
		));
	}
}

