<?php

class AjaxNewController extends BaseController {

	public function postBuku(){
		if(Request::ajax()){
		    $rules = array(
				'cover'		=> 'mimes:jpg,jpeg,png|max:5000',
		        'judul'		=> 'required',
		        'penulis'	=> '',
		        'penerbit'  => '',
		        'thnterbit'	=> 'integer',
		        'jumlah'	=> 'required|integer'
		    );
			$validator = Validator::make(Input::all(), $rules);

			if($validator->fails()){
				return Response::json(array(
					'fail'	=> true,
					'msg'	=> 'Gagal menambahkan buku. validator'
					));
			} else {
				// upload cover
				if (Input::hasFile('cover')){

					$file = Input::file('cover');
					$cover = time().'.png';
					$file->move('public/uploads/cover', $cover);
					 
					$image = Image::make('public/uploads/cover/'.$cover)->resize(100, 100)->save('public/uploads/cover/thumb/'.$cover);
				} else {
					$cover = null;
				}
				$userdata = array(
						'cover'			=> $cover,
						'kategori_id'	=> Input::get('kategori_id'),
						'judul'			=> Input::get('judul'),
						'penulis'		=> Input::get('penulis'),
						'penerbit'		=> Input::get('penerbit'),
						'thnterbit'		=> Input::get('thnterbit'),
						'jumlah'		=> Input::get('jumlah')
					);
				if(Buku::create($userdata)) {  
					return Response::json(array(
						'success'	=> true,
						'msg'		=> 'Sukses menambahkan buku.'
						));
				} else {
					return Response::json(array(
						'fail'	=> true,
						'msg'	=> 'Gagal menambahkan buku.'
						));
				}
			}
		} else {
			return Response::json(array(
				'fail'	=> true,
				'msg'	=> 'Json required'
				));
		}
	}
}