<?php

class Buku extends Eloquent {

	protected $table = 'buku';

	protected $fillable = array('kategori_id', 'judul', 'isbn', 'cover', 'jumlah', 'penerbit', 'penulis', 'thnterbit');

    public function kategori()
    {
        return $this->belongsTo('Kategori');
    }
    public function pinjam()
    {
        return $this->hasMany('Pinjaman');
    }
}
