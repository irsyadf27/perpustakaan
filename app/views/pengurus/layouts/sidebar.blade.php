            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            @if(empty(Auth::pengurus()->user()->foto))
                                <img src="{{ asset('assets/img/avatar4.png') }}" class="img-circle" alt="{{ Auth::pengurus()->user()->username }}" />
                            @else
                                <img src="{{ asset(Auth::pengurus()->user()->foto) }}" class="img-circle" alt="{{ Auth::pengurus()->user()->username }}" />
                            @endif
                        </div>
                        <div class="pull-left info">
                            <p>Hello, {{ Auth::pengurus()->user()->username }}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="{{ URL::to('pengurus') }}">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('pengurus/buku') }}">
                                <i class="fa fa-book"></i> <span>Buku</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('pengurus/kategori') }}">
                                <i class="fa fa-tag"></i> <span>Kategori</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('pengurus/siswa') }}">
                                <i class="fa fa-user"></i> <span>Siswa</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('pengurus/pengurus') }}">
                                <i class="fa fa-user"></i> <span>Pengurus</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('pengurus/setting') }}">
                                <i class="fa fa-gear"></i> <span>Pengaturan</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>