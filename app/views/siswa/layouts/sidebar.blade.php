            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            @if(empty(Auth::siswa()->user()->foto))
                                <img src="{{ asset('assets/img/avatar4.png') }}" class="img-circle" alt="{{ Auth::siswa()->user()->username }}" />
                            @else
                                <img src="{{ asset(Auth::siswa()->user()->foto) }}" class="img-circle" alt="{{ Auth::siswa()->user()->username }}" />
                            @endif
                        </div>
                        <div class="pull-left info">
                            <p>{{ Auth::siswa()->user()->nama }}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>