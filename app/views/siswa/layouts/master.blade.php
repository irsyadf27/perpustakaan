<!DOCTYPE html>
<html>
	@include('siswa.layouts.head', array('page_title' => (isset($page_title) ? $page_title : '')))
	<body class="skin-blue">
		@include('siswa.layouts.header')
		@include('siswa.layouts.sidebar')
		{{ $body }}
	</body>
</html>