<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengurusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pengurus', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('username', 10);
			$table->string('nama', 50);
			$table->string('email', 20);
			$table->string('password', 64);
			$table->string('foto', 30)->nullable();
			
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pengurus');
	}

}
