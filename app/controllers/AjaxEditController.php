<?php

class AjaxEditController extends \BaseController {

	function postAjaxbuku(){
		if(Request::ajax()){
			$id_buku = Input::get('pk');
			$name = Input::get('name');
			$value = Input::get('value');

			$buku = Buku::find($id_buku);
			switch($name){
				case 'buku_judul':
					$buku->judul = $value;
				break;
				case 'buku_kategori':
					$buku->kategori_id = $value;
				break;
				case 'buku_penerbit':
					$buku->penerbit = $value;
				break;
				case 'buku_penulis':
					$buku->penulis = $value;
				break;
				case 'buku_jumlah':
					$buku->jumlah = $value;
				break;
			}
			$buku->save();
		}
	}
}
