<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
	return View::make('hello');
});

Route::get('cek', function() {
	return Hash::make('270497');
});

/* Filter */
Route::filter('pengurus', function(){
	if (Auth::pengurus()->guest()) return Redirect::guest('pengurus/login');
});
Route::filter('siswa', function() {
	if (Auth::siswa()->guest()) return Redirect::guest('login');
});

/* Login Siswa */
Route::get('login', array('uses' => 'LoginController@showSiswa'));
Route::post('login', array('before'  =>  'csrf', 'uses' => 'LoginController@doSiswa'));

/* Login pengurus */
Route::get('pengurus/login', array('uses' => 'LoginController@showPengurus'));
Route::post('pengurus/login', array('before' => 'csrf', 'uses' => 'LoginController@doPengurus'));

/* Logout */
Route::get('logout', array('uses' => 'LoginController@doLogout'));

/* Group */
Route::group(array('before' => 'pengurus'), function() {	/* Ajax */
	/* Pengurus */
	Route::controller('ajax/new', 'AjaxNewController');
	Route::controller('ajax/edit', 'AjaxEditController');
	Route::controller('pengurus', 'PengurusController');
});

Route::group(array('before' => 'siswa'), function() {
	/* Siswa */
	Route::controller('/', 'SiswaController');
});