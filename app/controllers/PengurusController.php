<?php

class PengurusController extends BaseController {
	protected $layout = 'pengurus.layouts.master';

	function __construct(){
		
	}

	public function getIndex(){
		$this->layout->page_title = 'Dashboard | Perpustakaan';
		$this->layout->body = View::make('pengurus.dashboard');
	}

	/* Pengurus dengan method get */
	public function getPengurus(){
		$data = Pengurus::paginate(10);
		$this->layout->page_title = 'Daftar Pengurus | Perpustakaan';
		$this->layout->body = View::make('pengurus.pengurus')->with('data', $data);
	}

	/* Siswa dengan method get */
	public function getSiswa(){
		$data = Siswa::paginate(10);
		$this->layout->page_title = 'Daftar Siswa | Perpustakaan';
		$this->layout->body = View::make('pengurus.siswa')->with('data', $data);
	}

	/* Kategori dengan method get */
	public function getKategori(){
		$data = Kategori::paginate(10);
		$this->layout->page_title = 'Kategori | Perpustakaan';
		$this->layout->body = View::make('pengurus.kategori')->with('data', $data);
	}

	/* Buku dengan method get */
	public function getBuku(){
		$data = Buku::paginate(10);
		$this->layout->page_title = 'Daftar Buku | Perpustakaan';
		$this->layout->body = View::make('pengurus.buku')->with('data', $data);
	}

	/* Setting dengan method get */
	public function getSetting(){
		$data = Setting::find(1);
		$this->layout->page_title = 'Pengaturan | Perpustakaan';
		$this->layout->body = View::make('pengurus.setting')->with('data', $data);
	}
}
