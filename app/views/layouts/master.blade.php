<!DOCTYPE html>
<html class="bg-black">
	@include('layouts.head', array('page_title' => (isset($page_title) ? $page_title : '')))
	{{ $body }}
</html>