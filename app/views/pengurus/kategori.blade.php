            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Kategori Buku
                        <small>Perpustakaan Digital</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> <a href="dashboard.php">Dashboard</a></li>
                        <li class="active">Kategori</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Input addon -->
                            <div class="box box-info">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-plus-square-o"></i> Tambah Kategori</h3>
                                </div>
                                <div class="box-body">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">Nama Kategori</span>
                                        <input type="text" class="form-control" placeholder="Nama Kategori">
                                        <span class="input-group-btn">
                                            <button class="btn btn-md btn-success btn-flat pull-right" title="Save"><i class="fa fa-save"></i> Simpan</button>
                                        </span>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-tag"></i> Kategori Buku</h3>
                                    <div class="margin box-tools">

                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-danger btn-flat pull-right" title="Delete"><i class='fa fa-trash-o'></i> Delete (kategori yg dipilih)</button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="tblSiswa" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center"><input type="checkbox"/></th>
                                                <th class="text-center">Nama Kategori</th>
                                                <th class="text-center">Jumlah Buku</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $kategori)
                                            <tr>
                                                <td class="text-center"><input type="checkbox" name="kategori[]" value="{{ $kategori['id'] }}" class="chkbox flat-red"/></td>
                                                <td><a href="#" id="{{ $kategori['id'] }}" class="dataEdit" data-type="text" data-pk="{{ $kategori['id'] }}" data-url="test.php" data-name="kategori_nama" data-title="Nama Kategori">{{ $kategori['nama'] }}</a></td>
                                                <td class="text-center">{{ $kategori->buku->count() }}</td>
                                                <td class="text-center">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-flat btn-info" title="Edit"><i class='fa fa-pencil'></i> Edit</button>
                                                        <button type="button" class="btn btn-flat btn-danger" title="Delete"><i class='fa fa-trash-o'></i> Delete</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="box-footer clearfix">
                                        @if($data->getLastPage() > 1)
                                        <ul class="pagination pagination-sm no-margin pull-right">
                                            <?php
                                            $presenter = new Illuminate\Pagination\BootstrapPresenter($data);
                                            echo $presenter->render();
                                            ?>
                                        </ul>
                                        @endif
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        @include('pengurus.layouts.footer')
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $.fn.editable.defaults.mode = 'inline';
                $('.dataEdit').editable();
                $('#tblSiswa').dataTable({
                    "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 0, 3 ] }],
                    "bPaginate": false,
                    "bLengthChange": true,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": true
                });
            });
        </script>