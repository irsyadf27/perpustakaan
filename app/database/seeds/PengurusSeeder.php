<?php

class PengurusSeeder extends Seeder{

	public function run(){
		DB::table('pengurus')->delete();
		Pengurus::create(array(
			'username' => 'admin',
			'nama'     => 'Web Administrator',
			'email'    => 'root@wmail.my.id',
			'password' => Hash::make('123456'),
		));
	}
}

