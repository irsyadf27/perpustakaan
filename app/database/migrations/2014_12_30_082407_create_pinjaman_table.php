<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePinjamanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pinjaman', function(Blueprint $table)
		{
			$table->increments('id');

			//$table->integer('jumlah');
			$table->date('tgl_pinjam');
			$table->date('tgl_kembali');
			$table->integer('status');

			$table->integer('buku_id')->unsigned();
			$table->foreign('buku_id')->references('id')->on('buku')->onDelete('CASCADE')->onUpdate('CASCADE');

			$table->integer('siswa_id')->unsigned();
			$table->foreign('siswa_id')->references('id')->on('siswa')->onDelete('CASCADE')->onUpdate('CASCADE');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pinjaman');
	}

}
