<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBukuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('buku', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('kategori_id')->unsigned();
			$table->foreign('kategori_id')->references('id')->on('kategori');

			$table->string('judul', 100);
			$table->string('isbn', 20)->nullable();
			$table->string('cover', 30)->nullable();
			$table->integer('jumlah');
			$table->string('penulis', 100)->nullable();
			$table->string('penerbit', 50)->nullable();
			$table->integer('thnterbit')->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('buku');
	}

}
