            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Halaman Informasi Perpustakaan Digital</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-book"></i> Daftar Buku Pinjaman</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-striped table-hover">
                                        <tr>
                                            <th>Judul Buku</th>
                                            <th>Kategori/Kelas</th>
                                            <th>Tanggal Peminjaman</th>
                                            <th>Tanggal Pengembalian</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                        @foreach($buku as $data)
                                        <tr>
                                            <td>{{ $data->judul}}</td>
                                            <td>{{ $data->nama_kategori }}</td>
                                            <td>{{ $data->tgl_pinjam }}</td>
                                            <td>{{ $data->tgl_kembali }}</td>
                                            <td class="text-center">
                                                @if($data->status == 0 && date('Y-m-d') > $data->tgl_kembali)
                                                    <span class="label label-danger">Belum Dikembalikan (Denda)</span>
                                                @elseif($data->status == 0)
                                                    <span class="label label-danger">Belum Dikembalikan</span>
                                                @else
                                                    <span class="label label-success">Sudah Dikembalikan</span>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div><!-- /.box-body -->
                                <div class="box-footer clearfix">
                                    @if($buku->getLastPage() > 1)
                                    <ul class="pagination pagination-sm no-margin pull-right">
                                        <?php
                                        $presenter = new Illuminate\Pagination\BootstrapPresenter($buku);
                                        echo $presenter->render();
                                        ?>
                                    </ul>
                                    @endif
                                </div>
                            </div><!-- /.box -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box box-primary">
                                <div class="box-body no-padding">
                                    <!-- THE CALENDAR -->
                                    <div id="calendar"></div>
                                </div><!-- /.box-body -->
                            </div><!-- /. box -->
                        </div><!-- /.col -->
                        <div class="col-md-6">
                            <div class="box box-info">
                                <div class="box-header">
                                    <i class="fa fa-bullhorn"></i>
                                    <h3 class="box-title">Pengumuman</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <div class="callout callout-danger">
                                        <h4>I am a danger callout!</h4>
                                        <p>There is a problem that we need to fix. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                                    </div>
                                    <div class="callout callout-info">
                                        <h4>I am an info callout!</h4>
                                        <p>Follow the steps to continue to payment.</p>
                                    </div>
                                    <div class="callout callout-warning">
                                        <h4>I am a warning callout!</h4>
                                        <p>This is a yellow callout.</p>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- /.col -->
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        @include('siswa.layouts.footer')
        <!-- Page specific script -->
        <script type="text/javascript">
            $(function() {
                $('#calendar').fullCalendar({
                    header: {
                        center: 'title',
                        left: '',
                        right: 'prev,next'
                    },
                    events: [
                        @foreach($buku as $tgl)
                        {
                            title: '{{ $tgl->judul }}',
                            start: '{{ $tgl->tgl_pinjam }}',
                            end: '{{ $tgl->tgl_pinjam }}',
                            allDay: true,
                            backgroundColor: "#00a65a",
                            borderColor: "#00a65a"
                        },
                        {
                            title: '{{ $tgl->judul }}',
                            start: '{{ $tgl->tgl_kembali }}',
                            end: '{{ $tgl->tgl_kembali }}',
                            allDay: true,
                            backgroundColor: "#f56954",
                            borderColor: "#f56954"
                        },
                        @endforeach
                    ]
                });

            });
        </script>