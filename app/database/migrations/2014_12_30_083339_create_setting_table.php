<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('setting', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('logo', 30)->nullable();
			$table->string('nama', 50);
			$table->string('telp', 50)->nullable();
			$table->string('email', 50)->nullable();
			$table->string('alamat');

			$table->integer('denda')->default(1000);
			$table->integer('denda_perhari')->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('setting');
	}

}
