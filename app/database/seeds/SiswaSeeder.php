<?php

class SiswaSeeder extends Seeder{

	public function run(){
		DB::table('siswa')->delete();
		Siswa::create(array(
			'nis'		=> '123456',
			'nama'		=> 'Siswa Teladan',
			'kelas'		=> 'XII IPA 100',
			'jeniskelamin' => 'L',
			'telp'		=> '081234567890',
			'password'	=> Hash::make('123456'),
		));
	}
}

