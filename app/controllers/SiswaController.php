<?php

class SiswaController extends BaseController {
	protected $layout = 'siswa.layouts.master';

	function __construct(){
		
	}

	public function getIndex(){
		$data = Siswa::join('pinjaman', 'pinjaman.siswa_id', '=', 'siswa.id')
					->join('buku', 'buku.id', '=', 'pinjaman.buku_id')
					->join('kategori', 'kategori.id', '=', 'buku.kategori_id')
					->select(
						'siswa.nama as namas', 'buku.judul',
						'kategori.nama as nama_kategori',
						'pinjaman.tgl_pinjam',
						'pinjaman.tgl_kembali',
						'pinjaman.status'
					)
					->where('siswa.id', '=', Auth::siswa()->user()->id)
					->paginate(10);
		$this->layout->page_title = "Dashboard | Perpustakaan";
		$this->layout->body = View::make('siswa.dashboard')->with('buku', $data);
	}
}
