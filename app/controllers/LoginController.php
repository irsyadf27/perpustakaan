<?php

class LoginController extends BaseController {
	protected $layout = 'layouts.master';

	public function __construct() {
		$this->beforeFilter('csrf', array('on'=>'post'));
	}

	/* login siswa */
	public function showSiswa()
	{
		if(Auth::siswa()->check()){
			return Redirect::to('/');
		}
		$this->layout->page_title = 'Siswa | Log in';
		$this->layout->body = View::make('login', array(
			'login_url' => 'login',
			'login_header' => 'Log in Siswa',
			'input_user' => 'nis',
			'sekolah' => Setting::find(1)->nama
		));
	}
	public function doSiswa()
	{
		if(Auth::siswa()->check()){
			return Redirect::to('/');
		}
		// validator
		$rules = array(
			'nis' => 'required|numeric',
			'password' => 'required|min:6'
		);
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator)
				->withInput(Input::except('password')); 
		} else {
			// create our user data for the authentication
			$userdata = array(
				'nis' => Input::get('nis'),
				'password' => Input::get('password')
			);
			// attempt to do the login
			if (Auth::siswa()->attempt($userdata, Input::get('remember_me'))) {
				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				return Redirect::to('/');
			} else {
				$errors = ['error' => ['NIS and/or Password invalid.']]; 
				// validation not successful, send back to form
				return Redirect::to('login')
					->withErrors($errors)
					->withInput(Input::except('password')); 
			}
		}
	}

	/* login pengurus */
	public function showPengurus()
	{
		if(Auth::pengurus()->check()){
			return Redirect::to('pengurus');
		}
		$this->layout->page_title = 'Pengurus | Log in';
		$this->layout->body = View::make('login', array(
			'login_url' => 'pengurus/login',
			'login_header' => 'Log in Pengurus',
			'input_user' => 'username',
			'sekolah' => Setting::find(1)->nama
		));
	}
	public function doPengurus()
	{
		if(Auth::pengurus()->check()){
			return Redirect::to('pengurus');
		}
		// validator
		$rules = array(
			'username' => 'required',
			'password' => 'required|min:6'
		);
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('pengurus/login')
				->withErrors($validator)
				->withInput(Input::except('password')); 
		} else {
			// create our user data for the authentication
			$userdata = array(
				'username' => Input::get('username'),
				'password' => Input::get('password')
			);
			// attempt to do the login
			if (Auth::pengurus()->attempt($userdata, Input::get('remember_me'))) {
				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				//$this->layout->body = '';
				return Redirect::to('pengurus');
			} else {
				$errors = ['error' => ['Username and/or Password invalid.']]; 
				// validation not successful, send back to form
				return Redirect::to('pengurus/login')
					->withErrors($errors)
					->withInput(Input::except('password')); 
			}
		}
	}

	/* Logout */
	public function doLogout()
	{
		Auth::siswa()->logout();
		Auth::pengurus()->logout();
		return Redirect::to('/');
	}
}
